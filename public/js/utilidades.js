function validarCedula(cedula) {
    var cad = cedula.trim();
    var total = 0;
    var longitud = cad.length;
    var longcheck = longitud - 1;

    if (cad !== "" && longitud === 10) {
        for (i = 0; i < longcheck; i++) {
            if (i % 2 === 0) {
                var aux = cad.charAt(i) * 2;
                if (aux > 9)
                    aux -= 9;
                total += aux;
            } else {
                total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
            }
        }

        total = total % 10 ? 10 - total % 10 : 0;

        if (cad.charAt(longitud - 1) == total) {
            return true;
        } else {
            return false;
        }
    }
}
function random(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
let valueAppearances = [];

for (let index = 0; index < 10; index++) {
    valueAppearances.push(0);
}
for (let i = 0; i <= 10000; i++) {
    let randomIndex = random(2, 9);
    valueAppearances[randomIndex]++;
}
console.log(valueAppearances);


