'use strict';
var cuentaC = required('../../modelos/cuenta');
var personaC = required('../../modelos/persona');
/** 
 * @class Cuenta_Controller
 */
class Cuenta_Controller {

    /**  
     * @description Metodo para El Inicio de Sesion
     * @param {req} req  Para tratar los datos del Back end
     * @param {res} res  Parapresentar los datos en  Front end
     * @property {object} Determinamos si la cuenta existe , para luego preguntar si lso datos ingresados son correctos.
     */
    iniciar_sesion(req, res) {
        cuentaC.getJoin({
            persona: true
        }).filter({
            correo: req.body.correo
        }).run().then(function (andrea) {


            if (andrea.length > 0) {
                console.log(andrea);
                var cuenta = andrea[0];
                console.log(cuenta.persona.id_rol + "  dasdas");
                personaC.getJoin({
                    rol: true
                }).filter({
                    id_rol: cuenta.persona.id_rol
                }).run().then(function (faican) {
                    var persona = faican[0];


                    if (cuenta.clave === req.body.contra) {
                        /*
                         * Guarda datos de session de la persona que se registro
                         */

                        req.session.datos = {
                            id: cuenta.id_persona,
                            usuario: (cuenta.persona.apellidos + " " + cuenta.persona.nombres),
                            rol: persona.rol.tipo_rol
                        };
                        console.log("entro correctamente ");



                        if (persona.rol.tipo_rol == 'Administrador') {
                            //Permite redirigir a la pagina principal del Administrador  
                            res.redirect('/Admin');


                        } else if (persona.rol.tipo_rol == 'Usuario') {
                            //Permite redirigir a la pagina principal del Usuario
                            res.redirect("/usuario");


                        }

                    } else {
                        req.flash('error', 'Sus credenciales son incorrectas!');
                        res.redirect('/');

                    }

                });
            } else {



                req.flash('error', 'Sus credenciales son incorrectas!');

                res.redirect('/');
            }
        });


    }
    /**  
     * @description Metodo para El Cerar Sesion
     * @param {req} req  Para tratar los datos del Back end
     * @param {res} res  Parapresentar los datos en  Front end
     * @returns  Cuando cerramos secion nos direccionamos ala principal
     */


    cerrar_sesion_tiempo(req, res) {
        req.flash('info', 'Se cerro el sistema debido a la inactividad!');
        req.session.destroy();
        res.redirect('/');
    }
    
    invalidar_sesion_tiempo(req, res) {
        req.session.destroy('30000');
        res.redirect('/');
    }
    var password = document.getElementById("nueva contraseña");
    var confirm_password = document.getElementById("Confirmar contraseña");
    function almacenar_contra() {
        if (password.value != confirm_password.value) {
            // definir la función nameDisplayCheck()
            function nameDisplayCheck() {
                // verifica si el elemento de datos 'name' está guardado en el almacenamiento web
                if (localStorage.getItem('name')) {
                    // Si es así, muestra un saludo personalizado
                    let name = localStorage.getItem('name');
                    h1.textContent = 'Bienvenido, ' + name;
                    personalGreeting.textContent = '¡Bienvenido a nuestro sitio web, ' + name +
                    '! Esperamos que te diviertas mientras estés aquí.';
                    // ocultar la parte 'recordar' del formulario y mostrar la parte 'olvidar'
                    forgetDiv.style.display = 'block';
                    rememberDiv.style.display = 'none';
                } else {
                    // si no, muestra un saludo genérico
                    h1.textContent = 'Bienvenido a nuestro sitio web ';
                    personalGreeting.textContent = 'Bienvenido a nuestro sitio web. Esperamos que se diviertas mientras estés aquí.';
                    // ocultar la parte 'olvidar' del formulario y mostrar la parte 'recordar'
                    forgetDiv.style.display = 'none';
                    rememberDiv.style.display = 'block';
                }
            }
        } else {
            confirm_password.setCustomValidity('');
            console.log("Contraseña cambiada");
        }
    }    
    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

module.exports = Cuenta_Controller;