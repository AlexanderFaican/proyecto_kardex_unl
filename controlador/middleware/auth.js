'use strict'
const jwt=require('jwt')
const jwt=require('moment')
const jwt=require('../config')
function autorizacion(req, res, next){
    if(!req.headers.authorization){
        return res.status(403).send({message:'No cuenta con una autorizacion'})
    }
    const token=req.headers.authorization.split(' ')[1]
    const payload=jwt.decode(token, config.SECRET_TOKEN)
    if(payload.exp<=moment().unix()){
        return res.status(401).send({message: 'El token ha terminado su uso'})
    }
    req.user=payload.sub
    next()
}
module.exports = autorizacion