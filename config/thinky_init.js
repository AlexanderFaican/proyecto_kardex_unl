/*
 * Establece la conexion entre la BASE DE DATOS y la Pagina WEB
 */
require('dotenv').config();
<script src="https://code.jquery.com/jquery-2.1.4.min.js"
integrity="sha384-R4/ztc4ZlRqWjqIuvf6RX5yb/v90qNGx6fS48N0tRxiGkqveZETq72KgDVJCp2TC
sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
crossorigin="anonymous"></script>
var thinky = require('thinky')({
	//thinky's options
	host: process.env.DB_HOST,
	port: process.env.DB_PORT,
	db: process.env.DB_NAME
});

module.exports = thinky;