const express = require('express');
const app = express();
var persona = require('../modelos/persona');
var controladorPersona = require('../controlador/controladorPersona');
var persona2 = new controladorPersona();
// controlador Administrador
var controladoradmin = require('../controlador/ControladorAdministrador');
var Empresa = new controladoradmin();
//para iniciar session
var cuentaC = require('../controlador/CuentaController');
var cuenta = new cuentaC();
app.get('/', (req, res) => {
  res.send('Controll Express.js!');
});

const PORT = process.env.PORT || 3002;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});
module.exports = app;